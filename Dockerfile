FROM debian:stable-20211201-slim

RUN apt-get update \
        && apt-get install -y --no-install-recommends \
                ed \
                less \
                locales \
                vim-tiny \
                wget \
                ca-certificates \
                fonts-texgyre

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
        && locale-gen en_US.utf8 \
        && /usr/sbin/update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

RUN apt-get update
RUN apt-get install -y r-base r-base-dev r-recommended
RUN apt-get install -y r-cran-forecast r-cran-mcmcpack r-cran-rsolnp r-cran-deoptim r-cran-nloptr r-cran-data.table

RUN R -e "install.packages('ConsReg');     if (!library(ConsReg, logical.return=T)) quit(status=10)"
COPY bmodel.R /usr/local/
RUN ln -s /usr/local/bmodel.R /usr/local/bin/bmodel
